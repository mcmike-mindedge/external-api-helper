# external-api-helper

[![npm version][npm-version-src]][npm-version-href]
[![npm downloads][npm-downloads-src]][npm-downloads-href]
[![Circle CI][circle-ci-src]][circle-ci-href]
[![Codecov][codecov-src]][codecov-href]
[![Dependencies][david-dm-src]][david-dm-href]
[![Standard JS][standard-js-src]][standard-js-href]

> A plugin To Wrap 3rd Party Apis

[📖 **Release Notes**](./CHANGELOG.md)

## Setup

1. Add the `external-api-helper` dependency with `yarn` or `npm` to your project
2. Add `external-api-helper` to the `modules` section of `nuxt.config.js`
3. Configure it:

```js
{
  modules: [
    // Simple usage
    'external-api-helper',

    // With options
    ['external-api-helper', { /* module options */ }]
  ]
}
```

## Development

1. Clone this repository
2. Install dependencies using `yarn install` or `npm install`
3. Start development server using `npm run dev`

## License

[MIT License](./LICENSE)

Copyright (c) Mike McGrath <mmcgrath@mindedge.com>

<!-- Badges -->
[npm-version-src]: https://img.shields.io/npm/dt/external-api-helper.svg?style=flat-square
[npm-version-href]: https://npmjs.com/package/external-api-helper

[npm-downloads-src]: https://img.shields.io/npm/v/external-api-helper/latest.svg?style=flat-square
[npm-downloads-href]: https://npmjs.com/package/external-api-helper

[circle-ci-src]: https://img.shields.io/circleci/project/github/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git.svg?style=flat-square
[circle-ci-href]: https://circleci.com/gh/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git

[codecov-src]: https://img.shields.io/codecov/c/github/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git.svg?style=flat-square
[codecov-href]: https://codecov.io/gh/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git

[david-dm-src]: https://david-dm.org/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git/status.svg?style=flat-square
[david-dm-href]: https://david-dm.org/https://mcmike-mindedge@bitbucket.org/mcmike-mindedge/external-api-helper.git

[standard-js-src]: https://img.shields.io/badge/code_style-standard-brightgreen.svg?style=flat-square
[standard-js-href]: https://standardjs.com
